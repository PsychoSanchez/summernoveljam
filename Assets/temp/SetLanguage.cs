﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SetLanguage : MonoBehaviour 
{
    public void setLanguage(string language)
    {
        PlayerPrefs.SetString(Settings.LOCALIZATION_LANGUAGE, language);

        SceneManager.LoadScene("Main Menu");
    }
}
