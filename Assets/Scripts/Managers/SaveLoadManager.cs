﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System;

public class SaveLoadManager : MonoBehaviour 
{
    [SerializeField] GameObject[] save_slots;
    [SerializeField] CanvasGroup canvas;
    [SerializeField] Text state_label;

    string state; //i'd rather use enum, but buttons can't pass enum parms

    void Awake()
    {
        DontDestroyOnLoad(this.gameObject);

        for (int i = 1; i <= save_slots.Length; i++)
        {
            save_slots[i - 1].GetComponentInChildren<Text>().text = i.ToString() + ". " + PlayerPrefs.GetString(Saves.SAVESLOT + i.ToString());
        }
    }

    public void openSaveLoadMenu(string state)
    {
        this.state = state;

        state_label.text = state;

        canvas.alpha = 1;
        canvas.interactable = true;
        canvas.blocksRaycasts = true;

        switch(state)
        {
            //logic to draw appropriate buttons goes here
            case "Load":
                Debug.Log("Loading mod");
                break;

            case "Save":
                Debug.Log("Saving mod");
                break;
        }
    }

    public void closeSaveLoadMenu()
    {
        canvas.alpha = 0;
        canvas.interactable = false;
        canvas.blocksRaycasts = false;
    }

    public void operate(string saveslot_number)
    {
        string saveslot = Saves.SAVESLOT + saveslot_number;

        switch(state)
        {
            case "Load":
                loadGame(saveslot);
                break;

            case "Save":
                saveGame(saveslot);
                break;
        }
    }

    void saveGame(string saveslot)
    {
        Messenger.Broadcast(Messages.SAVING_GAME); // tells GameManager to get current game scene and save it in playerprefs

        string current_scene = PlayerPrefs.GetString(Saves.SCENE_TO_SAVE);

        PlayerPrefs.SetString(saveslot, current_scene);

        string saveslot_number = saveslot[saveslot.Length - 1].ToString();

        save_slots[Int32.Parse(saveslot_number) - 1].GetComponentInChildren<Text>().text = saveslot_number + ". " + PlayerPrefs.GetString(Saves.SAVESLOT + saveslot_number);
    }
        
    public void loadGame(string saveslot)
    {
        string scene_to_load = PlayerPrefs.GetString(saveslot);
        PlayerPrefs.SetString(Saves.SCENE_TO_LOAD, scene_to_load);

        closeSaveLoadMenu();

        SceneManager.LoadScene(Scenes.GAME);
    }
}
