﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Fungus;

public class GameManager : MonoBehaviour 
{
    [SerializeField] Flowchart flowchart;

    void Start()
    {
        string scene_to_load = PlayerPrefs.GetString(Saves.SCENE_TO_LOAD);
        flowchart.ExecuteBlock(scene_to_load);

        Messenger.AddListener(Messages.SAVING_GAME, onSavingGame);
    }

    void onSavingGame()
    {
        string current_scene = flowchart.GetExecutingBlocks()[0].BlockName;

        PlayerPrefs.SetString(Saves.SCENE_TO_SAVE, current_scene);
    }
}
