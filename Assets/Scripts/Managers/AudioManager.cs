﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour 
{
    [SerializeField] AudioSource music_source;
    [SerializeField] AudioSource sound_source;

    void Awake()
    {
        DontDestroyOnLoad(this.gameObject);
        music_source.volume = PlayerPrefs.GetFloat(Settings.MUSIC_VOLUME);
        sound_source.volume = PlayerPrefs.GetFloat(Settings.SOUND_VOLUME);
    }

    public void setMusicVolume(float volume)
    {
        music_source.volume = volume;
    }

    public void setSoundVolume(float volume)
    {
        sound_source.volume = volume;
    }
}
