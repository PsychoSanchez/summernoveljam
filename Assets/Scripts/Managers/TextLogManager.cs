﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextLogManager : MonoBehaviour 
{
    [SerializeField] CanvasGroup canvas;
    [SerializeField] Text text;

    string previous_character_name;

    void Awake()
    {
        text.text = "";
        previous_character_name = "";
    }

    void Start()
    {
        Messenger.AddListener<string, string>(Messages.NEW_TEXT_ENTRY, addNewText);
    }

    public void openTextLog()
    {
        canvas.alpha = 1;
        canvas.interactable = true;
        canvas.blocksRaycasts = true;
    }

    public void closeTextLog()
    {
        canvas.alpha = 0;
        canvas.interactable = false;
        canvas.blocksRaycasts = false;
    }

    void addNewText(string character_name, string new_text)
    {
        if (character_name != previous_character_name)
        {
            text.text += "\n";
        }

        previous_character_name = character_name;

        if (character_name != null)
        {
            text.text = character_name + "\n";
        }

        text.text += new_text + " ";
    }
}
