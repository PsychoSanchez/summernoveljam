﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

/// <summary>
/// Contains methods to manipulate settings menu and message game about settings changes.
/// </summary>
public class SettingsController : MonoBehaviour 
{
    [SerializeField] Slider music_volume_slider;
    [SerializeField] Slider sound_volume_slider;
    [SerializeField] Toggle autoreader_toggle;
    [SerializeField] Slider autoreader_pause_slider;
    [SerializeField] Slider reading_speed_slider;

    [SerializeField] AudioManager Audio;

    [SerializeField] CanvasGroup canvas;

    private string language;
    private float music_volume;
    private float sound_volume;
    private string autoreader_state; // Playerprefs don't support boolean keys, therefore we use string
    private bool isAutoreaderEnabled; // We only need this to init autoreader_toggle value
    private float autoreader_pause;
    private float reading_speed;

    void Awake()
    {
        DontDestroyOnLoad(this.gameObject);

        language = PlayerPrefs.GetString(Settings.LOCALIZATION_LANGUAGE);
        music_volume = PlayerPrefs.GetFloat(Settings.MUSIC_VOLUME);
        sound_volume = PlayerPrefs.GetFloat(Settings.SOUND_VOLUME);
        autoreader_state = PlayerPrefs.GetString(Settings.AUTOREADER_STATE);
        isAutoreaderEnabled = autoreader_state == "disabled" ? false : true;
        autoreader_pause = PlayerPrefs.GetFloat(Settings.AUTOREADER_PAUSE);
        reading_speed = PlayerPrefs.GetFloat(Settings.READING_SPEED);

        music_volume_slider.value = music_volume;
        sound_volume_slider.value = sound_volume;
        autoreader_toggle.isOn = isAutoreaderEnabled;
        autoreader_pause_slider.value = autoreader_pause;
        reading_speed_slider.value = reading_speed;
    }

    public void openSettingsMenu()
    {
        canvas.alpha = 1;
        canvas.interactable = true;
        canvas.blocksRaycasts = true;

        Messenger.Broadcast(Messages.SETTINGS_OPENED);
    }

    public void closeSettingsMenu()
    {
        // store data in PlayerPrefs and broadcast gamesystems to update their values
        PlayerPrefs.SetString(Settings.LOCALIZATION_LANGUAGE, language);
        PlayerPrefs.SetFloat(Settings.MUSIC_VOLUME, music_volume);
        PlayerPrefs.SetFloat(Settings.SOUND_VOLUME, sound_volume);
        PlayerPrefs.SetString(Settings.AUTOREADER_STATE, autoreader_state);
        PlayerPrefs.SetFloat(Settings.AUTOREADER_PAUSE, autoreader_pause);
        PlayerPrefs.SetFloat(Settings.READING_SPEED, reading_speed);

        PlayerPrefs.Save();

        Messenger.Broadcast(Messages.SETTINGS_CLOSED);

        canvas.alpha = 0;
        canvas.interactable = false;
        canvas.blocksRaycasts = false;
    }


    public void changeLanguage(string language)
    {
        this.language = language;

        Messenger.Broadcast(Messages.LANGUAGE_CHANGED, language); // every text with ContentLanguageController component will receive this message 
    }

    public void setMusicVolume(float volume)
    {
        music_volume = volume;
        Audio.setMusicVolume(volume);
    }

    public void setSoundVolume(float volume)
    {
        sound_volume = volume;
        Audio.setSoundVolume(volume);
    }

    public void switchAutoreader(bool isEnabled)
    {
        autoreader_state = isEnabled ? "enabled" : "disabled";
    }

    public void setAutoreaderPause(float time)
    {
        autoreader_pause = time;
    }

    public void setReadingSpeed(float speed)
    {
        reading_speed = speed;
    }
}
