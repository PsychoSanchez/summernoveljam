﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

/// <summary>
/// Changes language of content based on playerprefs key "Language". 
/// </summary>
public class ContentLanguageController : MonoBehaviour 
{
    [SerializeField] string russian_content;
    [SerializeField] string english_content;


    void changeContentLanguage(string language = null)
    {
        Text content = GetComponent<Text>();

        //if language parm is provided
        if (language != null)
        {
            if (language == "Russian")
            {
                content.text = russian_content;
            }
            else
            {
                content.text = english_content;
            }

        }
        //otherwise look for language in playerprefs
        else
        {
            if (PlayerPrefs.GetString(Settings.LOCALIZATION_LANGUAGE) == "Russian")
            {
                content.text  = russian_content;
            }
            else
            {
                content.text = english_content;
            }
        }
    }

    void Start()
    {
        Messenger.AddListener<string>(Messages.LANGUAGE_CHANGED, changeContentLanguage);

        Messenger.MarkAsPermanent(Messages.LANGUAGE_CHANGED);

        changeContentLanguage();
    }

    void OnDestroy()
    {
        Messenger.RemoveListener<string>(Messages.LANGUAGE_CHANGED, changeContentLanguage);
    }
}
