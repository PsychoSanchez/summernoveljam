﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Fungus;

public class AutoreaderPauseController : MonoBehaviour
{
    [SerializeField] Flowchart flowchart;

    float wait_time;

    void Awake()
    {
        updateAutoreaderPause();
    }
    void Start()
    {
        Messenger.AddListener(Messages.SETTINGS_CLOSED, updateAutoreaderPause);
    }

    void updateAutoreaderPause()
    {
        wait_time = PlayerPrefs.GetFloat(Settings.AUTOREADER_PAUSE);

        wait_time = PlayerPrefs.GetString(Settings.AUTOREADER_STATE) == "enabled" ? wait_time : 0;

        flowchart.SetFloatVariable("wait_time", wait_time);
    }
}

