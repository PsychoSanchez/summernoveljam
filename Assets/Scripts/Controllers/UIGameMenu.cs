﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class UIGameMenu : MonoBehaviour 
{
    [SerializeField] CanvasGroup canvas;
    [SerializeField] TextLogManager TextLog;

    SettingsController _Settings;
    SaveLoadManager SaveLoad;

    bool isOpened;

    void Awake()
    {
        isOpened = false;
        PlayerPrefs.SetString(Settings.GAME_MENU_STATE, "OFF");

        _Settings = GameObject.FindWithTag(Tags.SETTINGS_MENU).GetComponent<SettingsController>();
        SaveLoad = GameObject.FindWithTag(Tags.SAVELOAD_MENU).GetComponent<SaveLoadManager>();
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Tab))
        {
            isOpened = !isOpened;

            if (isOpened)
            {
                openGameMenu();
            }

            else
            {
                closeGameMenu();
            }
        }
    }
    public void openGameMenu()
    {
//        Messenger.Broadcast(Messages.GAME_MENU_OPENED);
        PlayerPrefs.SetString(Settings.GAME_MENU_STATE, "ON");

        canvas.alpha = 1;
        canvas.interactable = true;
        canvas.blocksRaycasts = true;

    }

    public void closeGameMenu()
    {
        canvas.alpha = 0;
        canvas.interactable = false;
        canvas.blocksRaycasts = false;

        _Settings.closeSettingsMenu();
        SaveLoad.closeSaveLoadMenu();
        TextLog.closeTextLog();

//        Messenger.Broadcast(Messages.GAME_MENU_CLOSED);
        PlayerPrefs.SetString(Settings.GAME_MENU_STATE, "OFF");
    }

    public void openSettings()
    {
        _Settings.openSettingsMenu();
    }

    public void openSaveLoadMenu(string state)
    {
        SaveLoad.openSaveLoadMenu(state);
    }

    public void openMainMenu()
    {
        SceneManager.LoadScene(Scenes.MAIN_MENU);
    }
}
