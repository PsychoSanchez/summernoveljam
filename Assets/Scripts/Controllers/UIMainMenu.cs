﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

/// <summary>
/// Contains methods to navigate through main menu.
/// All button pannels should be linked to this script's gameObject.
/// Each button should have a pointer to another panel.
/// </summary>
public class UIMainMenu : MonoBehaviour 
{
    public void startGame()
    {
        PlayerPrefs.SetString(Saves.SCENE_TO_LOAD, "Start");

        SceneManager.LoadScene(Scenes.GAME);
    }

    public void exitGame()
    {
        Application.Quit();
    }
}
