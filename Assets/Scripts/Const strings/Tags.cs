﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Tags 
{
    public const string MUSIC_SOURCE = "Music Source";
    public const string SOUND_SOURCE = "Sound Source";

    public const string SETTINGS_MENU = "Settings Menu";
    public const string SAVELOAD_MENU = "SaveLoad Menu";
}
