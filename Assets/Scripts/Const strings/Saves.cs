﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Saves 
{
    public const string SCENE_TO_LOAD = "SCENE_TO_LOAD";
    public const string SCENE_TO_SAVE = "SCENE_TO_SAVE";

    public const string SAVESLOT = "SAVESLOT_";
    public const string SAVESLOT_1 = "SAVESLOT_1";
    public const string SAVESLOT_2 = "SAVESLOT_2";
    public const string SAVESLOT_3 = "SAVESLOT_3";
    public const string SAVESLOT_4 = "SAVESLOT_4";
    public const string SAVESLOT_5 = "SAVESLOT_5";
    public const string SAVESLOT_6 = "SAVESLOT_6";
    public const string SAVESLOT_7 = "SAVESLOT_7";
    public const string SAVESLOT_8 = "SAVESLOT_8";
    public const string SAVESLOT_9 = "SAVESLOT_9";
}
