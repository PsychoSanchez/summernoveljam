﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Scenes 
{
    public const string INTRO = "Intro";
    public const string MAIN_MENU = "Main Menu";
    public const string GAME = "Game";
}
