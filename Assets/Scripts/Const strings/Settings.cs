﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Contains const strings representing Playerprefs keys. Each key represents settings menu option.
/// </summary>
public static class Settings 
{
    public const string LOCALIZATION_LANGUAGE = "LOCALIZATION_LANGUAGE";
    public const string MUSIC_VOLUME = "MUSIC_VOLUME";
    public const string SOUND_VOLUME = "SOUND_VOLUME";
    public const string AUTOREADER_STATE = "AUTOREADER_STATE";
    public const string AUTOREADER_PAUSE = "AUTOREADER_PAUSE";
    public const string READING_SPEED = "READING_SPEED";

    public const string GAME_MENU_STATE = "GAME_MENU_STATE";
}
