﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Contains const strings representing messages of Messenger system.
/// </summary>
public static class Messages 
{
    public const string SETTINGS_OPENED = "SETTINGS_OPENED";
    public const string SETTINGS_CLOSED = "SETTINGS_CLOSED";

    public const string GAME_MENU_OPENED = "GAME_MENU_OPENED";
    public const string GAME_MENU_CLOSED = "GAME_MENU_CLOSED";

    public const string LANGUAGE_CHANGED = "LANGUAGE_CHANGED";

    public const string SAVING_GAME = "SAVING_GAME";
    public const string GAME_SAVED = "GAME_SAVED";
    public const string GAME_LOADED = "GAME_LOADED";

    public const string NEW_TEXT_ENTRY = "NEW_TEXT_ENTRY";
}
