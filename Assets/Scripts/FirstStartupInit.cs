﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Initialize PlayerPrefs values if player enters the game for the first time;
/// </summary>
public class FirstStartupInit : MonoBehaviour 
{
    void Awake()
    {
        if (!PlayerPrefs.HasKey("Played"))
        {
            PlayerPrefs.SetString("Played", "yes");

            PlayerPrefs.SetString(Settings.LOCALIZATION_LANGUAGE, "English"); // "Russian" / "English"
            PlayerPrefs.SetFloat(Settings.MUSIC_VOLUME, 1f); // from 0f to 1f (0% - 100%)
            PlayerPrefs.SetFloat(Settings.SOUND_VOLUME, 1f); // ^
            PlayerPrefs.SetString(Settings.AUTOREADER_STATE, "disabled"); // "enabled" / "disabled"
            PlayerPrefs.SetFloat(Settings.AUTOREADER_PAUSE, 0); // pause between scenes in seconds (works only if autoreader is enabled)
            PlayerPrefs.SetFloat(Settings.READING_SPEED, 20f); // speed in chrs / s
        }
    }
}